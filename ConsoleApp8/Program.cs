﻿using System;

namespace ConsoleApp8
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rand = new Random();
            int nrAles = rand.Next(100);

            int nr;
            bool ghicit = false;
            int incercari = 0;

            while (ghicit == false)
            {
                Console.WriteLine("Ghiceste numarul:");
                while (int.TryParse(Console.ReadLine(), out nr)) ;
                if (nr > 100)
                {
                    Console.WriteLine("Numarul nu este valid!");
                    Console.Write("Introduceti un numar nou:");
                }
                incercari++;
                if (nr == nrAles)
                {
                    Console.WriteLine("Bravo ati ghicit! Din " + incercari + "incercari");
                    ghicit = true;
                }
                else

                {
                    int x = Math.Abs(nrAles - nr);
                    if (x <= 3)
                    {
                        Console.WriteLine("Foarte ferbinte");
                    }
                    else if (x <= 5)
                    {
                        Console.WriteLine("Ferbinte");
                    }
                    else if (x <= 10)
                    {
                        Console.WriteLine("Cald");
                    }
                    else if (x <= 20)
                    {
                        Console.WriteLine("Caldut");
                    }
                    else if (x <= 50)
                    {
                        Console.WriteLine("Rece");
                    }
                    else
                    {
                        Console.WriteLine("Foarte Rece");
                    }
                }
                Console.WriteLine();

            }
            Console.ReadKey();
        }
    }
}
